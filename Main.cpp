
#include "Hello_VTKM_GL_Interop.h"

#include <vtkm/opengl/internal/OpenGLHeaders.h>
#include <vtkm/cont/Timer.h>

#include <GL/glut.h>

const vtkm::UInt32 HEIGHT = 600;
const vtkm::UInt32  WIDTH = 900;

void run() {

    static Hello_VTKM_GL_Interop< VTKM_DEFAULT_DEVICE_ADAPTER_TAG, vtkm::Float32 > glTest(
        WIDTH,
        HEIGHT,
        < path to the shader source >
    );

    static vtkm::cont::Timer<> timer;
    
    glTest.renderFrame(timer.GetElapsedTime());

    glutSwapBuffers();
}

void idle() {
    glutPostRedisplay();
}

int main(int argc, char** argv) {

    glutInit(&argc, argv);
    glutInitWindowSize ( WIDTH, HEIGHT );
    glutInitDisplayMode ( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutCreateWindow("vtkm GL Interop Test");
    
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        std::cout << "glewInit failed\n";
    }

    glutDisplayFunc( run );
    glutIdleFunc(idle);
    glutMainLoop();

    return EXIT_SUCCESS;
}
