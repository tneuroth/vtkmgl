#version 330

layout(location = 0) in vec3 posAttr;
uniform mat4 MVP;

void main(void)
{
    vec4 pos = vec4( posAttr, 1.0 );
    gl_Position = MVP * pos;
}
