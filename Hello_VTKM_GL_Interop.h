#ifndef BFIELD_INTERP
#define BFIELD_INTERP

#include "LoadShaders.h"

#include <iostream>

#include <glm/gtc/matrix_transform.hpp> 

#include <vtkm/opengl/internal/OpenGLHeaders.h>
#include <vtkm/opengl/TransferToOpenGL.h>

#include <vtkm/Math.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/Field.h>
#include <vtkm/cont/ArrayHandleUniformPointCoordinates.h>

template< typename DeviceAdapter, typename T >
struct Hello_VTKM_GL_Interop {
    
    GLuint programID;
    GLuint vbo;
    GLuint vao;

    glm::mat4 view;
    glm::mat4 proj;
    glm::mat4 modelMat;

    vtkm::cont::ArrayHandle< vtkm::Vec< T, 3 > > out;
    vtkm::cont::ArrayHandle< vtkm::Vec< T, 3 > > in;

    vtkm::Vec< vtkm::UInt32, 2 > xzDim;

    struct GenerateSurfaceWorklet : public vtkm::worklet::WorkletMapField {

        vtkm::Float32 t;
        GenerateSurfaceWorklet() : t(0.f) {}

        VTKM_CONT_EXPORT
        void set_t( vtkm::Float32 _t ) {
            t = _t;
        }

        typedef void ControlSignature( FieldIn<>, FieldOut<> );
        typedef void ExecutionSignature( _1, _2 );

        VTKM_EXEC_EXPORT
        void operator()( const vtkm::Vec< T, 3 > & input, vtkm::Vec<T, 3> & output ) const
        {
            output[0] = input[0];
            output[1] = .2f * vtkm::Sin( input[0] * 10.f + t ) * vtkm::Cos( input[2] * 10.f + t );
            output[2] = input[2];
        }
    };

    Hello_VTKM_GL_Interop( 
        vtkm::UInt32 width, 
        vtkm::UInt32 height,
        const std::string & srcDir )
    {
        std::vector< vtkm::Vec< T, 3 > > data;
        vtkm::UInt32 dim = 100;

        for (int i = 0; i < dim; ++i ) {
            for (int j = 0; j < dim; ++j ) {
                data.push_back( vtkm::Vec<T,3>( 2.f * i / dim - 1.f, 0.f, 2.f * j / dim - 1.f ) );
            }
        }

        xzDim = vtkm::Vec< vtkm::UInt32, 2 >( dim, dim );

        vtkm::cont::ArrayHandle< vtkm::Vec< T, 3 > > tmp = vtkm::cont::make_ArrayHandle(data);
        vtkm::cont::DeviceAdapterAlgorithm< VTKM_DEFAULT_DEVICE_ADAPTER_TAG >::Copy(tmp, in);

        modelMat = glm::mat4(1.f);

        view = glm::lookAt(
            glm::vec3( 0.f, 2.f, 2.f ), 
            glm::vec3( 0.f, 0.f, 0.f  ),   
            glm::vec3( 0.f, 1.f, 0.f  )        
        );

        proj = glm::perspective( 
            45.0f, 
            (float)( width/height ), 
            0.1f, 
            10.0f 
        );

        glGenVertexArrays( 1, &vao );
        glBindVertexArray( vao );

        programID = LoadShaders(  ( srcDir + "basic.vert" ).c_str(), ( srcDir + "basic.frag" ).c_str() );
        glUseProgram( programID );

        glClearColor( .08, .08, .08, 0.f );
        glViewport(0, 0, width, height );
        glPointSize(2.f);
    }

    ~Hello_VTKM_GL_Interop() {

        glDeleteBuffers(1, &vbo);
        glDeleteVertexArrays(1, &vao);
        glDeleteProgram(programID);
    }

    void render() {

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        vtkm::UInt32 sz = xzDim[0] * xzDim[1];
    
        glm::mat4 MVP = proj*view*modelMat;
        GLuint uid = glGetUniformLocation( programID, "MVP");
        glUniformMatrix4fv( uid, 1, GL_FALSE, &MVP[0][0] );

        glEnableVertexAttribArray(0);       
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
        
        glDrawArrays( GL_POINTS, 0, sz );
        
        glDisableVertexAttribArray(0);
    }

    void renderFrame( vtkm::Float32 t ) 
    {
        typedef vtkm::worklet::DispatcherMapField<GenerateSurfaceWorklet> DispatcherType;
        
        modelMat = glm::rotate(glm::mat4(1.f), t / 10.f, glm::vec3(0.f, 1.f, 0.f));

        GenerateSurfaceWorklet worklet;
        worklet.set_t( t );
        
        DispatcherType(worklet).Invoke( in, out );

        vtkm::opengl::TransferToOpenGL(out, vbo, DeviceAdapter());

        render();
    }
};

#endif

